import Text.ParserCombinators.ReadP
import Control.Applicative ((<|>))
import Data.Char
import Data.List (intercalate)

data Type
    = Record String [(String, Type)]
    | Custom String [(String, [Type])]
    | String
    | Bool
    | Int
    | Float
    | UserDefined String
    | ParseError
    deriving Show

main :: IO ()
main = do
    let inputModule = "Ports"
        inputType = "ClientMsg"
        outputModule = "ClientMsg"
    type_ <- parseType inputModule inputType
    let header = fileHeader outputModule inputType
        encoder = generateEncoder type_ inputType
        decoder = generateDecoder type_ inputType
    writeFile (outputModule ++ ".elm") $ header ++ encoder ++ decoder
    putStrLn $ show type_

parseType :: String -> String -> IO Type
parseType fileName typeName = do
    contents <- readFile $ fileName ++ ".elm"
    let parseResult = readP_to_S (typeParser typeName) contents
        type_ = case parseResult of
            [] -> ParseError
            ((result, _):_) -> result
    case type_ of
        Record _ fields -> fmap (Record $ fileName ++ "." ++ typeName) $ mapM (expandUserDefinedField fileName) fields
        Custom _ constructors -> fmap (Custom $ fileName ++ "." ++ typeName) $ mapM (expandUserDefinedConstructor fileName) constructors
        _ -> expandUserDefinedType fileName type_

expandUserDefinedType :: String -> Type -> IO Type
expandUserDefinedType currentFile (UserDefined qualifiedName) =
    let parts = qualifiedName `splitOn` '.'
        (fileName, typeName) = findType currentFile parts
    in parseType fileName typeName
expandUserDefinedType _ type_ = return type_

findType :: String -> [String] -> (String, String)
findType currentFile parts =
    let path = init parts
        typeName = last parts
    in
        if path == [] then
            (currentFile, typeName)
        else
            (intercalate "/" path, typeName)

expandUserDefinedField :: String -> (String, Type) -> IO (String, Type)
expandUserDefinedField fileName (fieldName, type_) = do
    expandedType <- expandUserDefinedType fileName type_
    return (fieldName, expandedType)

expandUserDefinedConstructor :: String -> (String, [Type]) -> IO (String, [Type])
expandUserDefinedConstructor fileName (tag, values) = do
    expandedValues <- mapM (expandUserDefinedType fileName) values
    return (tag, expandedValues)

typeParser :: String -> ReadP Type
typeParser typeName = do
    skipAll
    customTypeParser typeName <|> aliasTypeParser typeName

skipAll :: ReadP ()
skipAll = skipMany $ satisfy $ const True

customTypeParser :: String -> ReadP Type
customTypeParser typeName = do
    string $ "type " ++ typeName
    fmap (Custom typeName) $ manyTill constructorParser customEndParser

constructorParser :: ReadP (String, [Type])
constructorParser = do
    skipSpaces
    char '=' <|> char '|'
    char ' '
    tag <- munch1 isAlphaNum
    values <- many valueParser
    char '\n'
    return (tag, values)

valueParser :: ReadP Type
valueParser = do
    char ' '
    subTypeParser

customEndParser :: ReadP ()
customEndParser = do
    skipSpaces
    fmap (const ()) (satisfy isAlpha) <|> eof

aliasTypeParser :: String -> ReadP Type
aliasTypeParser typeName = do
    string $ "type alias " ++ typeName ++ " ="
    simpleTypeParser <|> recordTypeParser typeName

simpleTypeParser :: ReadP Type
simpleTypeParser = do
    skipSpaces
    subTypeParser

recordTypeParser :: String -> ReadP Type
recordTypeParser typeName =
    fmap (Record typeName) $ manyTill fieldParser recordEndParser

fieldParser :: ReadP (String, Type)
fieldParser = do
    skipSpaces
    char '{' <|> char ','
    char ' '
    name <- munch1 isAlphaNum
    string " : "
    type_ <- subTypeParser
    return (name, type_)

recordEndParser :: ReadP ()
recordEndParser = do
    skipSpaces
    fmap (const ()) $ char '}'

subTypeParser :: ReadP Type
subTypeParser =
    fmap parseSubType $ munch1 (\char -> isAlphaNum char || char == '.')

parseSubType :: String -> Type
parseSubType typeName =
    case typeName of
        "String" -> String
        "Bool" -> Bool
        "Int" -> Int
        "Float" -> Float
        _ -> UserDefined typeName

splitOn :: Eq a => [a] -> a -> [[a]]
splitOn [] _ = []
splitOn list separator =
    let (start, rest) = break (== separator) list
    in case rest of
        [] -> [start]
        (sep:end) -> start:splitOn end separator

fileHeader :: String -> String -> String
fileHeader moduleName typeName =
    "module " ++ moduleName ++ " exposing (encode" ++ typeName ++ ", " ++ toCamelCase typeName ++ "Decoder)\n\n\
    \import Json.Decode as D exposing (Decoder, fail, succeed)\n\
    \import Json.Encode as E exposing (Value)\n\
    \import Json.Decode.Pipeline exposing (requiredAt)\n\n\n"

toCamelCase :: String -> String
toCamelCase [] = []
toCamelCase (head:tail) = toLower head : tail

generateEncoder :: Type -> String -> String
generateEncoder type_ name =
    let variable = toCamelCase name
        header =
            "encode" ++ name ++ " : " ++ name ++ " -> Value\n\
            \encode" ++ name ++ " " ++ variable ++ " =\n"
    in case type_ of
        Record _ fields ->
            let fieldOutputs = map (fieldToEncoder variable) fields
                fieldEncoders = intercalate "\n        , " $ map fst fieldOutputs
                functions = concatMap snd fieldOutputs
            in header ++ "    E.object\n        [ " ++ fieldEncoders ++ "\n        ]\n\n\n" ++ functions
        Custom _ constructors ->
            let constructorOutputs = map constructorToEncoder constructors
                constructorCases = concatMap fst constructorOutputs
                functions = concatMap snd constructorOutputs
            in header ++ "    case " ++ variable ++ " of\n" ++ constructorCases ++ "\n" ++ functions

fieldToEncoder :: String -> (String, Type) -> (String, String)
fieldToEncoder variable (fieldName, type_) =
    let (encoder, function) = typeToEncoder type_
        fieldEncoder = "( \"" ++ fieldName ++ "\", " ++ encoder ++ " " ++ variable ++ "." ++ fieldName ++ " )"
    in
        (fieldEncoder, function)

constructorToEncoder :: (String, [Type]) -> (String, String)
constructorToEncoder (tag, data_) =
    let indexes = [1..length data_]
        dataVariable index = " var" ++ show index
        case_ = "        " ++ tag ++ concatMap dataVariable indexes ++ " ->\n"
        indent = replicate 16 ' '
        subIndent = replicate 24 ' '
        (dataEncoders, functions) = case data_ of
            [] -> ("", "")
            [value] ->
                let (typeEncoder, functions) = typeToEncoder value
                    encoder = indent ++ ", ( \"data\", " ++ typeEncoder ++ " var1 )\n"
                in (encoder, functions)
            _ ->
                let valueOutputs = map typeToEncoder data_
                    indexedEncoders = zip [1..] $ map fst valueOutputs
                    showDataEncoder (index, dataEncoder) = " ( \"" ++ show index ++ "\", " ++ dataEncoder ++ " var" ++ show index ++ " )\n" ++ subIndent
                    dataEncoders = intercalate "," $ map showDataEncoder indexedEncoders
                    functions = concatMap snd valueOutputs
                    constructorEncoder =
                        indent ++ ", ( \"data\"\n" ++
                        indent ++ "  , E.object\n" ++
                        subIndent ++ "[" ++ dataEncoders ++ "]\n" ++
                        indent ++ "  )\n"
                in (constructorEncoder, functions)
        object = "            E.object\n                [ ( \"tag\", E.string \"" ++ tag ++ "\" )\n" ++ dataEncoders ++ "                ]\n\n"
        body = case_ ++ object
    in (body, functions)

typeToEncoder :: Type -> (String, String)
typeToEncoder type_ =
    case type_ of
        Record typeName _ -> handleRecursiveType type_ typeName
        Custom typeName _ -> handleRecursiveType type_ typeName
        String -> ("E.string", "")
        Bool -> ("E.bool", "")
        Int -> ("E.int", "")
        Float -> ("E.float", "")
        _ -> ("", "")
    where
        handleRecursiveType type_ name =
            ("encode" ++ name, generateEncoder type_ name)

generateDecoder :: Type -> String -> String
generateDecoder type_ name =
    let camelCase = toCamelCase name
        header =
            camelCase ++ "Decoder : Decoder " ++ name ++ "\n" ++
            camelCase ++ "Decoder =\n"
    in case type_ of
        Record _ fields ->
            let firstLine = "    succeed " ++ name ++ "\n"
                fieldOutputs = map fieldToDecoder fields
                fieldDecoders = concatMap (("        " ++) . fst) fieldOutputs
                functions = concatMap snd fieldOutputs
            in header ++ firstLine ++ fieldDecoders ++ functions
        Custom _ constructors ->
            let start =
                    "    let\n\
                    \        handleTag tag =\n\
                    \            case tag of\n"
                constructorOutputs = map constructorToDecoder constructors
                constructorDecoders = concatMap fst constructorOutputs
                functions = concatMap snd constructorOutputs
                end =
                    "                _ ->\n\
                    \                    fail \"\"\n\
                    \    in\n\
                    \    D.field \"tag\" D.string |> D.andThen handleTag\n"
            in header ++ start ++ constructorDecoders ++ end ++ functions

fieldToDecoder :: (String, Type) -> (String, String)
fieldToDecoder (name, type_) =
    let quote str = "\"" ++ str ++ "\""
        accessor = "[ " ++ (intercalate ", " $ map quote $ name `splitOn` '.') ++ " ]"
        prefix = "|> requiredAt " ++ accessor ++ " "
    in case type_ of
        Record typeName _ -> handleRecursiveType prefix type_ typeName
        Custom typeName _ -> handleRecursiveType prefix type_ typeName
        String -> (prefix ++ "D.string\n", "")
        Bool -> (prefix ++ "D.bool\n", "")
        Int -> (prefix ++ "D.int\n", "")
        Float -> (prefix ++ "D.float\n", "")
        _ -> ("", "")
    where
        handleRecursiveType prefix type_ name =
            (prefix ++ toCamelCase name ++ "Decoder\n", generateDecoder type_ name)

constructorToDecoder :: (String, [Type]) -> (String, String)
constructorToDecoder (name, data_) =
    let prefix =
            "                \"" ++ name ++ "\" ->\n\
            \                    succeed " ++ name ++ "\n"
        indent = replicate 24 ' '
        (valueDecoders, functions) = case data_ of
            [] -> ("", "")
            [value] -> mapFst (indent ++) $ fieldToDecoder ("data", value)
            _ ->
                let indexedData = zip [1..] data_
                    valueToDecoder (index, value) = fieldToDecoder ("data." ++ show index, value)
                    valueOutputs = map valueToDecoder indexedData
                    valueDecoders = concatMap ((indent ++) . fst) valueOutputs
                    functions = concatMap snd valueOutputs
                in (valueDecoders, functions)
    in (prefix ++ valueDecoders, functions)
    where mapFst f (a, b) = (f a, b)
