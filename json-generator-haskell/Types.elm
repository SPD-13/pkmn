module Types exposing (Problem, Conflict)

import Data

type alias Problem =
    String

type NestedImport = NestedImport ManyValues

type Conflict
    = Conflict
    | OtherConflict

type ManyValues
    = Constructor String Bool Int Float String Bool Int Float ManyFields

type alias ManyFields =
    { one : String
    , two : Bool
    , three : Int
    , four : Float
    , five : String
    , six : Bool
    , seven : Int
    , eight : Float
    , nine : Data.Hello
    }
