port module Ports exposing (ClientMsg)

import Types

type alias ClientMsg =
    { from : String
    , to : String
    , data : ClientMsgData
    , conflict : Conflict
    , otherConflict : Types.Conflict
    }

type ClientMsgData
    = JoinRequest
    | JoinResponse JoinResponse
    | Test Types.NestedImport

type JoinResponse
    = Success
    | Failure Types.Problem

type Conflict = Conflict
