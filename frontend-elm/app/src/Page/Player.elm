module Page.Player exposing (Model, Msg(..), init, subscriptions, update, view)

import Css exposing (..)
import Design exposing (..)
import GameData exposing (..)
import Html.Styled exposing (Attribute, Html, button, div, h1, h2, img, input, p, span, styled, text)
import Html.Styled.Attributes as Attr exposing (..)
import Html.Styled.Events exposing (keyCode, on, onClick, onInput)
import Images exposing (Images)
import Json.Decode as Json
import Ports exposing (..)
import Sprites exposing (..)
import Types exposing (..)


type Model
    = EnteringCode RoomCode EnteringCodeStatus
    | EnteringInfo Step
    | WaitingForGame Bool Bool
    | GameStarted


type EnteringCodeStatus
    = Idle (Maybe Problem)
    | WaitingForServer
    | WaitingForHost


type Step
    = Intro Color
    | Gender Color (Maybe Gender)
    | Name Color Gender String
    | Avatar Color Gender String (Maybe Sprite)
    | Starter Color Gender String Sprite (Maybe Starter)


type Gender
    = Boy
    | Girl


type Msg
    = CodeChanged String
    | Play
    | SkipIntro
    | AssumeGender Gender
    | ConfirmGender
    | NameChanged String
    | ConfirmName
    | AvatarChanged Sprite
    | ConfirmAvatar
    | ChooseStarter Starter
    | CancelStarter
    | ConfirmStarter
    | ServerMsg FromServerMsg
    | ClientMsg ClientMsg
    | NoOp


init : Model
init =
    EnteringCode "" <| Idle Nothing


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case ( msg, model ) of
        ( CodeChanged code, EnteringCode _ status ) ->
            if String.length code > 4 then
                ( model, Cmd.none )

            else
                ( EnteringCode code status, Cmd.none )

        ( Play, EnteringCode code (Idle _) ) ->
            ( EnteringCode code WaitingForServer, sendServerMsg <| RoomRequest <| String.toUpper code )

        ( SkipIntro, EnteringInfo (Intro color) ) ->
            ( EnteringInfo <| Gender color Nothing, Cmd.none )

        ( AssumeGender gender, EnteringInfo (Gender color _) ) ->
            ( EnteringInfo <| Gender color <| Just gender, Cmd.none )

        ( ConfirmGender, EnteringInfo (Gender color (Just gender)) ) ->
            ( EnteringInfo <| Name color gender "", Cmd.none )

        ( NameChanged name, EnteringInfo (Name color gender _) ) ->
            if String.length name > 16 then
                ( model, Cmd.none )

            else
                ( EnteringInfo <| Name color gender name, Cmd.none )

        ( ConfirmName, EnteringInfo (Name color gender name) ) ->
            if String.isEmpty name then
                ( model, Cmd.none )

            else
                ( EnteringInfo <| Avatar color gender name Nothing, Cmd.none )

        ( AvatarChanged sprite, EnteringInfo (Avatar color gender name _) ) ->
            ( EnteringInfo <| Avatar color gender name <| Just sprite, Cmd.none )

        ( ConfirmAvatar, EnteringInfo (Avatar color gender name (Just sprite)) ) ->
            ( EnteringInfo <| Starter color gender name sprite Nothing, Cmd.none )

        ( ChooseStarter starter, EnteringInfo (Starter color gender name avatar _) ) ->
            ( EnteringInfo <| Starter color gender name avatar <| Just starter, Cmd.none )

        ( CancelStarter, EnteringInfo (Starter color gender name avatar (Just _)) ) ->
            ( EnteringInfo <| Starter color gender name avatar Nothing, Cmd.none )

        ( ConfirmStarter, EnteringInfo (Starter color gender name avatar (Just starter)) ) ->
            ( WaitingForGame False False, Cmd.none )

        ( ServerMsg message, _ ) ->
            case ( message, model ) of
                ( RoomResponse clientID, EnteringCode code WaitingForServer ) ->
                    ( EnteringCode code WaitingForHost, sendClientMsg clientID JoinRequest )

                ( Error error, EnteringCode code WaitingForServer ) ->
                    ( EnteringCode code <| Idle <| Just error, Cmd.none )

                ( _, _ ) ->
                    ( model, Cmd.none )

        ( ClientMsg { from, to, data }, _ ) ->
            case ( data, model ) of
                ( JoinResponse joinResponse, EnteringCode code WaitingForHost ) ->
                    case joinResponse of
                        Success color ->
                            ( EnteringInfo <| Intro <| hex color, Cmd.none )

                        Failure problem ->
                            ( EnteringCode code <| Idle <| Just problem, Cmd.none )

                ( _, _ ) ->
                    ( model, Cmd.none )

        ( _, _ ) ->
            ( model, Cmd.none )


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ onServerMsg (receiveServerMsg >> ServerMsg)
        , onClientMsg (receiveClientMsg >> handleClientMsg)
        ]


handleClientMsg : Maybe ClientMsg -> Msg
handleClientMsg maybeMessage =
    case maybeMessage of
        Just message ->
            ClientMsg message

        Nothing ->
            NoOp


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Json.succeed msg

            else
                Json.fail ""
    in
    on "keydown" (Json.andThen isEnter keyCode)


view : Images -> Model -> Html Msg
view images model =
    case model of
        EnteringCode code status ->
            viewLandingPage images code status

        EnteringInfo step ->
            viewInfoPage images step

        WaitingForGame _ _ ->
            viewWaitingPage

        _ ->
            div [] []


viewLandingPage images code status =
    let
        playButton =
            buttonPlay [ type_ "button", onClick Play, Attr.disabled (String.length code /= 4) ] [ text "Play" ]

        playElements =
            case status of
                Idle Nothing ->
                    [ playButton ]

                Idle (Just problem) ->
                    [ pProblem [] [ text problem ], playButton ]

                _ ->
                    [ buttonPlay [ type_ "button", Attr.disabled True ] [ text "Loading..." ] ]
    in
    divLayout []
        [ div []
            [ imgLogo [ src images.logo ] []
            , h1SubTitle [] [ text "The drinking game" ]
            ]
        , divCodeContainer []
            [ h1RoomCode [] [ text "Room Code" ]
            , inputCode [ value code, maxlength 4, onInput CodeChanged, onEnter Play ] []
            ]
        , div [] playElements
        ]


screenLayout =
    Css.batch
        [ displayFlex
        , flexDirection column
        , textAlign center
        , Css.property "justify-content" "space-evenly"
        , Css.height (pct 100)
        ]


mainButton =
    Css.batch
        [ Css.width (pct 60)
        , fontSize (em 1.5)
        , font
        , margin (em 0)
        , padding (em 0.4)
        , backgroundColor colors.buttonOk
        , border (px 0)
        , borderRadius (em 2)
        , cursor pointer
        , Css.disabled [ opacity (num 0.5) ]
        ]


divLayout =
    styled div [ screenLayout ]


divCodeContainer =
    styled div [ margin2 (em 2) (em 0) ]


h1RoomCode =
    styled h1 [ margin4 (em 0) (em 0) (em 0.2) (em 0) ]


inputCode =
    styled input
        [ fontSize (em 3)
        , font
        , Css.width (em 2.6)
        , textAlign center
        , padding (em 0.15)
        , textTransform uppercase
        , border3 (px 2) solid (hex "000000")
        ]


pProblem =
    styled p
        [ fontSize (em 1.4)
        , margin (em 0)
        , lineHeight (em 2)
        , color colors.errorText
        ]


buttonPlay =
    styled button [ mainButton ]


imgLogo =
    styled img [ Css.width (pct 80), margin (em 0) ]


h1SubTitle =
    styled h1 [ marginBottom (em 0) ]


viewInfoPage : Images -> Step -> Html Msg
viewInfoPage images step =
    case step of
        Intro _ ->
            viewIntro images

        Gender _ maybeGender ->
            viewGender images maybeGender

        Name _ _ name ->
            viewName name

        Avatar color gender name maybeAvatar ->
            viewAvatar images color gender name maybeAvatar

        Starter color _ name _ maybeStarter ->
            viewStarter images color name maybeStarter


viewIntro images =
    divInfo []
        [ divOakContainer []
            [ divNidoran images [] []
            , divOak images [] []
            ]
        , pIntro []
            [ text
                """Hello, there!
                Welcome to the world of POKÉMON!
                My name is OAK.
                This world is inhabited far and wide by creatures called POKÉMON.
                But first, tell me a little about yourself."""
            ]
        , nextButton [ type_ "button", onClick SkipIntro ] [ text "Next" ]
        ]


divInfo =
    styled div [ screenLayout, justifyContent spaceBetween ]


divOakContainer =
    styled div
        [ position relative
        , Css.height (em 12)
        , Css.width (em 18)
        , margin auto
        ]


divOak images =
    styledHtmlFromSprite
        [ top (em 6)
        , left (em 11)
        , transform <| scale 2
        ]
        images
        (TrainerSprite 1 3)


divNidoran images =
    styledHtmlFromSprite
        [ top (em 8)
        , left (em 7)
        , transform <| scale 2
        ]
        images
        (PokemonSprite 7 4)


pIntro =
    styled p [ padding2 (em 0) (em 2), lineHeight (em 1.5), margin (em 0) ]


nextButton =
    styled button [ mainButton, margin2 (em 1.6) auto ]


viewGender images maybeGender =
    divInfo []
        [ divGenders []
            [ divGenderContainer [ onClick <| AssumeGender Boy ]
                [ (if maybeGender == Just Boy then
                    spanGenderMaleSelected

                   else
                    spanGenderMale
                  )
                    []
                    [ text "♂" ]
                , spanGenderName [] [ text "BOY" ]
                ]
            , divGenderContainer [ onClick <| AssumeGender Girl ]
                [ (if maybeGender == Just Girl then
                    spanGenderFemaleSelected

                   else
                    spanGenderFemale
                  )
                    []
                    [ text "♀" ]
                , spanGenderName [] [ text "GIRL" ]
                ]
            ]
        , pIntro []
            [ text
                """Now tell me.
                Are you a boy?
                Or are you a girl?"""
            ]
        , nextButton [ type_ "button", Attr.disabled (maybeGender == Nothing), onClick ConfirmGender ] [ text "Next" ]
        ]


divGenders =
    styled div [ margin auto ]


divGenderContainer =
    styled div
        [ Css.width (em 10)
        , margin2 (em 0) auto
        , transform <| scale 0.8
        , cursor pointer
        ]


genderIconStyle =
    Css.batch
        [ display inlineBlock
        , Css.width (em 1.125)
        , Css.height (em 1.125)
        , lineHeight (em 1.125)
        , border3 (em 0.0625) solid colors.white
        , borderRadius (pct 50)
        , fontSize (pct 800)
        ]


spanGenderMale =
    styled span [ genderIconStyle, backgroundColor (hex "80d8ff"), borderColor (hex "e1f5fe") ]


spanGenderFemale =
    styled span [ genderIconStyle, backgroundColor (hex "ff80ab"), borderColor (hex "fce4ec") ]


spanGenderMaleSelected =
    styled span [ genderIconStyle, backgroundColor (hex "80d8ff"), borderColor colors.dark ]


spanGenderFemaleSelected =
    styled span [ genderIconStyle, backgroundColor (hex "ff80ab"), borderColor colors.dark ]


spanGenderName =
    styled span [ fontSize (em 2) ]


viewName name =
    divInfo []
        [ divNameContainer []
            [ pName [] [ text "YOUR NAME?" ]
            , inputName [ value name, maxlength 16, onInput NameChanged, onEnter ConfirmName ] []
            ]
        , pIntro []
            [ text
                """Let's begin with your name.
                What is it?"""
            ]
        , nextButton [ type_ "button", Attr.disabled (name == ""), onClick ConfirmName ] [ text "Next" ]
        ]


divNameContainer =
    styled div
        [ Css.width (pct 60)
        , margin auto
        ]


pName =
    styled p
        [ margin2 (em 0.2) (em 0)
        , textAlign left
        ]


inputName =
    styled input
        [ Css.width (pct 100)
        , Css.height (em 2)
        , padding2 (em 1.2) (em 0.8)
        , boxSizing borderBox
        , font
        , fontSize (em 1.1)
        ]


viewAvatar images color gender name maybeAvatar =
    let
        sprites =
            if gender == Boy then
                maleSprites

            else
                femaleSprites
    in
    divInfo []
        [ divSprites [] <| List.map (viewTrainerSprite images color maybeAvatar) sprites
        , pIntro [] [ text <| "Right... So your name is " ++ name ++ ". What do you look like?" ]
        , nextButton [ type_ "button", Attr.disabled (maybeAvatar == Nothing), onClick ConfirmAvatar ] [ text "Next" ]
        ]


viewTrainerSprite images playerColor maybeAvatar sprite =
    let
        color =
            case maybeAvatar of
                Nothing ->
                    colors.white

                Just avatar ->
                    if avatar == sprite then
                        playerColor

                    else
                        colors.white
    in
    divSpriteContainer [ onClick <| AvatarChanged sprite ] [ divSpriteBackground color [] [], htmlFromSprite images sprite [] [] ]


divSprites =
    styled div [ margin2 auto (em 0) ]


divSpriteContainer =
    styled div
        [ Css.width (pct 25)
        , Css.height (em 5)
        , display inlineBlock
        , position relative
        , cursor pointer
        ]


divSpriteBackground color =
    styled div
        [ Css.width (em 5)
        , Css.height (em 5)
        , borderRadius (pct 50)
        , backgroundColor color
        , margin auto
        ]


viewStarter images color name maybeStarter =
    let
        ( starterContainer, buttons ) =
            case maybeStarter of
                Nothing ->
                    ( divStarterContainer [] []
                    , [ styled nextButton [ visibility Css.hidden ] [] [ text "Next" ] ]
                    )

                Just starter ->
                    ( divStarterContainerFull color [] [ styled (htmlFromStarter images starter First) [ transform <| scale 3 ] [] [] ]
                    , [ noButton [ type_ "button", onClick CancelStarter ] [ text "NO" ]
                      , yesButton [ type_ "button", onClick ConfirmStarter ] [ text "YES" ]
                      ]
                    )

        ball starter isMiddle =
            divBallContainer isMiddle [] [ divBall images isMiddle [ onClick <| ChooseStarter starter ] [] ]
    in
    divInfo []
        [ divStarterChoice []
            [ ball Bulbasaur False
            , ball Charmander True
            , ball Squirtle False
            , starterContainer
            ]
        , styled pIntro [ Css.height (em 4.5) ] [] [ text <| starterText name maybeStarter ]
        , div [] buttons
        ]


starterText name maybeStarter =
    case maybeStarter of
        Nothing ->
            "Go on, choose a POKÉMON!"

        Just starter ->
            case starter of
                Bulbasaur ->
                    "So, " ++ name ++ ", you want to go with the GRASS POKÉMON BULBASAUR?"

                Charmander ->
                    "So, " ++ name ++ ", you're claiming the FIRE POKÉMON CHARMANDER?"

                Squirtle ->
                    "So, " ++ name ++ ", you've decided on the WATER POKÉMON SQUIRTLE?"


divStarterChoice =
    styled div [ margin auto, Css.width (pct 60) ]


divBallContainer isMiddle =
    styled div
        [ Css.width
            (pct <|
                if isMiddle then
                    40

                else
                    30
            )
        , Css.height (em 8)
        , position relative
        , display inlineBlock
        ]


divBall images isMiddle =
    styledHtmlFromSprite
        [ top
            (em <|
                if isMiddle then
                    2

                else
                    6
            )
        , transform (scale 2)
        , cursor pointer
        ]
        images
        (ItemSprite 3 1)


divStarterContainer =
    styled div
        [ position relative
        , Css.width (em 12)
        , Css.height (em 12)
        , margin auto
        , borderRadius (pct 50)
        ]


divStarterContainerFull color =
    styled divStarterContainer [ backgroundColor color ]


noButton =
    styled button
        [ mainButton
        , Css.width (pct 35)
        , margin2 (em 1.6) (pct 10)
        , backgroundColor colors.buttonCancel
        ]


yesButton =
    styled button
        [ mainButton
        , Css.width (pct 35)
        , margin4 (em 1.6) (pct 10) (em 1.6) (pct 0)
        ]


viewWaitingPage =
    divLayout [] [ h2 [] [ text "Waiting for other players" ] ]
