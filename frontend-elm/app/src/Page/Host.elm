module Page.Host exposing (Model, Msg(..), init, subscriptions, update, view)

import Array
import Css exposing (..)
import Design exposing (..)
import Html.Styled exposing (Html, div, h1, h2, img, p, styled, text)
import Html.Styled.Attributes exposing (..)
import Images exposing (Images)
import Ports exposing (..)
import Sprites exposing (..)
import Types exposing (..)


type Model
    = WaitingForRoom (Maybe Problem)
    | WaitingForPlayers RoomCode (List LobbyPlayer) Bool


type LobbyPlayer
    = Loading ClientID
    | Ready Player


type alias Player =
    { clientID : ClientID
    , name : String
    , avatar : Sprite
    , starter : Starter
    , color : Color
    , evolution : Evolution
    }


type Msg
    = ServerMsg FromServerMsg
    | ClientMsg ClientMsg
    | NoOp


maxPlayers : Int
maxPlayers =
    6


init : ( Model, Cmd msg )
init =
    ( WaitingForRoom Nothing, sendServerMsg HostRequest )


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        ServerMsg message ->
            case ( message, model ) of
                ( HostResponse roomCode, WaitingForRoom _ ) ->
                    ( WaitingForPlayers roomCode [] False, Cmd.none )

                ( Error error, WaitingForRoom _ ) ->
                    ( WaitingForRoom <| Just error, Cmd.none )

                ( _, _ ) ->
                    ( model, Cmd.none )

        ClientMsg { from, to, data } ->
            case ( data, model ) of
                ( JoinRequest, WaitingForPlayers roomCode players False ) ->
                    let
                        newState =
                            WaitingForPlayers roomCode (Loading from :: players) (List.length players + 1 == maxPlayers)

                        command =
                            sendClientMsg from <| JoinResponse <| Success <| Maybe.withDefault "000000" <| Array.get (List.length players) playerColors
                    in
                    ( newState, command )

                ( JoinRequest, _ ) ->
                    ( model, sendClientMsg from <| JoinResponse <| Failure "The game has already started" )

                ( _, _ ) ->
                    ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ onServerMsg (receiveServerMsg >> ServerMsg)
        , onClientMsg (receiveClientMsg >> handleClientMsg)
        ]


handleClientMsg : Maybe ClientMsg -> Msg
handleClientMsg maybeMessage =
    case maybeMessage of
        Just message ->
            ClientMsg message

        Nothing ->
            NoOp


view : Images -> Model -> Html Msg
view images model =
    case model of
        WaitingForRoom maybeProblem ->
            viewLoading maybeProblem

        WaitingForPlayers roomCode players ready ->
            viewRoomPage images roomCode players ready


viewLoading maybeProblem =
    let
        content =
            case maybeProblem of
                Nothing ->
                    "Loading..."

                Just problem ->
                    problem
    in
    divLayout [] [ h1 [] [ text content ] ]


viewRoomPage images roomCode players ready =
    divLayout []
        [ div []
            [ imgLogo [ src images.logo ] []
            , h1Title [] [ text "The drinking game" ]
            ]
        , div []
            [ h2Title [] [ text "Room Code" ]
            , pRoomCode [] [ text roomCode ]
            ]
        , div []
            [ divPlayers [] (List.map (viewPlayer images) players)
            , h2Title [] [ text (String.fromInt (List.length players) ++ "/6 players") ]
            ]
        ]


viewPlayer : Images -> LobbyPlayer -> Html Msg
viewPlayer images lobbyPlayer =
    case lobbyPlayer of
        Loading _ ->
            viewLoadingPlayer images

        Ready player ->
            viewReadyPlayer images player


viewLoadingPlayer images =
    divPlayer [] [ imgLoading [ src images.loading ] [] ]


viewReadyPlayer : Images -> Player -> Html Msg
viewReadyPlayer images player =
    divPlayer []
        [ divIcon player
            []
            [ divAvatar images player.avatar [] []
            , divStarter images player.starter player.evolution [] []
            ]
        , pName [] [ text player.name ]
        ]


divLayout =
    styled div
        [ displayFlex
        , flexDirection column
        , textAlign center
        , Css.property "justify-content" "space-evenly"
        , Css.height (pct 100)
        ]


imgLogo =
    styled img [ Css.width (pct 50), margin2 (em 0) auto ]


h1Title =
    styled h1 [ fontSize (em 2.8), margin (em 0) ]


h2Title =
    styled h2 [ fontSize (em 2), marginBottom (em 0.2) ]


pRoomCode =
    styled p
        [ fontSize (em 3)
        , border3 (px 2) solid (hex "000000")
        , display inlineBlock
        , margin (em 0)
        , padding2 (em 0.15) (em 0.35)
        ]


divPlayers =
    styled div
        [ displayFlex
        , justifyContent center
        , Css.height (em 10.4)
        ]


divPlayer =
    styled div [ margin2 (em 0) (em 4) ]


imgLoading =
    styled img [ Css.height (em 8), Css.property "filter" "invert(90%)" ]


pName =
    styled p [ fontSize (em 1.4), marginTop (em 0.2), marginBottom (em 0) ]


divIcon player =
    styled div
        [ position relative
        , backgroundColor player.color
        , Css.height (em 8)
        , Css.width (em 8)
        , borderRadius (pct 50)
        ]


divAvatar images avatar =
    styledHtmlFromSprite
        [ top (em 4)
        , left (em 2.5)
        ]
        images
        avatar


divStarter images starter evolution =
    styled (htmlFromStarter images starter evolution)
        [ top (em 4)
        , left (em 5.5)
        ]
