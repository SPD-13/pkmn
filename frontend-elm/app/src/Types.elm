module Types exposing (ClientID, Evolution(..), Problem, RoomCode, Starter(..))


type alias Problem =
    String


type alias ClientID =
    String


type alias RoomCode =
    String


type Starter
    = Bulbasaur
    | Charmander
    | Squirtle


type Evolution
    = First
    | Second
    | Third
