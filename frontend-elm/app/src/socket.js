import uuid from 'uuid/v1';

export class Socket {
    constructor(messageHandler) {
        this.socket = null;
        this.id = uuid();
        this.messageHandler = messageHandler;
    }

    connect(openHandler) {
        this.socket = new WebSocket(`ws://${window.location.hostname}:8080/ws?id=${this.id}`);
        this.socket.onopen = event => {
            if (openHandler)
                openHandler();
        };
        this.socket.onmessage = event => {
            console.log(event.data);
            this.messageHandler(JSON.parse(event.data));
        };
        this.socket.onclose = event => {
            console.log(event);
            console.log("WebSocket connection closed");
        };
    }

    send(message) {
        console.log(message);
        if (this.socket && this.socket.readyState === WebSocket.OPEN) {
            this.socket.send(JSON.stringify(message));
        }
        else {
            this.connect(() => {
                this.socket.send(JSON.stringify(message));
            });
        }
    }
};
