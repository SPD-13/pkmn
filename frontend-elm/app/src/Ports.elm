port module Ports exposing
    ( ClientMsg
    , ClientMsgData(..)
    , FromServerMsg(..)
    , JoinResponse(..)
    , ToServerMsg(..)
    , onClientMsg
    , onServerMsg
    , receiveClientMsg
    , receiveServerMsg
    , sendClientMsg
    , sendServerMsg
    )

import Json.Decode as D
import Json.Encode as E
import Types exposing (..)


type ToServerMsg
    = HostRequest
    | RoomRequest RoomCode


type FromServerMsg
    = HostResponse RoomCode
    | RoomResponse ClientID
    | Error Problem


type alias ClientMsg =
    { from : ClientID
    , to : ClientID
    , data : ClientMsgData
    }


type ClientMsgData
    = JoinRequest
    | JoinResponse JoinResponse


type JoinResponse
    = Success String
    | Failure Problem


sendServerMsg : ToServerMsg -> Cmd msg
sendServerMsg message =
    case message of
        HostRequest ->
            serverMsg <| E.object [ ( "hostRequest", E.bool True ) ]

        RoomRequest roomCode ->
            serverMsg <| E.object [ ( "joinRequest", E.object [ ( "code", E.string roomCode ) ] ) ]


sendClientMsg : ClientID -> ClientMsgData -> Cmd msg
sendClientMsg to data =
    let
        makeMessage message =
            clientMsg <| E.object [ ( "to", E.string to ), ( "data", E.object message ) ]
    in
    case data of
        JoinRequest ->
            makeMessage [ ( "tag", E.string "JoinRequest" ) ]

        JoinResponse joinResponse ->
            makeMessage [ ( "tag", E.string "JoinResponse" ), ( "data", sendJoinResponse joinResponse ) ]


sendJoinResponse : JoinResponse -> E.Value
sendJoinResponse joinResponse =
    case joinResponse of
        Success color ->
            E.object [ ( "tag", E.string "Success" ), ( "data", E.string color ) ]

        Failure problem ->
            E.object [ ( "tag", E.string "Failure" ), ( "data", E.string problem ) ]


receiveServerMsg : D.Value -> FromServerMsg
receiveServerMsg json =
    let
        decoded =
            D.decodeValue serverMsgDecoder json
    in
    case decoded of
        Ok message ->
            message

        Err error ->
            Error <| D.errorToString error


serverMsgDecoder : D.Decoder FromServerMsg
serverMsgDecoder =
    D.field "type" D.string |> D.andThen serverMsgTypeDecoder


serverMsgTypeDecoder : String -> D.Decoder FromServerMsg
serverMsgTypeDecoder messageType =
    case messageType of
        "HostResponse" ->
            hostResponseDecoder

        "JoinResponse" ->
            roomResponseDecoder

        _ ->
            D.fail "Unknown message type"


hostResponseDecoder : D.Decoder FromServerMsg
hostResponseDecoder =
    D.map HostResponse (D.field "room" D.string)


roomResponseDecoder : D.Decoder FromServerMsg
roomResponseDecoder =
    D.field "success" D.bool |> D.andThen roomSuccessDecoder


roomSuccessDecoder : Bool -> D.Decoder FromServerMsg
roomSuccessDecoder success =
    let
        message =
            D.field "message" D.string
    in
    if success then
        D.map RoomResponse message

    else
        D.map Error message


receiveClientMsg : D.Value -> Maybe ClientMsg
receiveClientMsg json =
    let
        decoded =
            D.decodeValue clientMsgDecoder json
    in
    case decoded of
        Ok message ->
            Just message

        Err _ ->
            Nothing


clientMsgDecoderError =
    "Error parsing client message"


clientMsgDecoder : D.Decoder ClientMsg
clientMsgDecoder =
    D.map3 ClientMsg (D.field "from" D.string) (D.field "to" D.string) (D.field "data" clientMsgDataDecoder)


clientMsgDataDecoder : D.Decoder ClientMsgData
clientMsgDataDecoder =
    let
        handleTag tag =
            case tag of
                "JoinRequest" ->
                    D.succeed JoinRequest

                "JoinResponse" ->
                    D.map JoinResponse (D.field "data" joinResponseDecoder)

                _ ->
                    D.fail clientMsgDecoderError
    in
    D.field "tag" D.string |> D.andThen handleTag


joinResponseDecoder : D.Decoder JoinResponse
joinResponseDecoder =
    let
        handleTag tag =
            case tag of
                "Success" ->
                    D.map Success (D.field "data" D.string)

                "Failure" ->
                    D.map Failure (D.field "data" D.string)

                _ ->
                    D.fail clientMsgDecoderError
    in
    D.field "tag" D.string |> D.andThen handleTag


port serverMsg : E.Value -> Cmd msg


port onServerMsg : (D.Value -> msg) -> Sub msg


port clientMsg : E.Value -> Cmd msg


port onClientMsg : (D.Value -> msg) -> Sub msg
