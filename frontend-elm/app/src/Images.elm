module Images exposing (Images)


type alias Images =
    { loading : String
    , logo : String
    , badges : String
    , custom : String
    , items : String
    , pokemon : String
    , trainers : String
    }
