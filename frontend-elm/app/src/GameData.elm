module GameData exposing (femaleSprites, maleSprites)

import Sprites exposing (..)


maleSprites =
    [ TrainerSprite 5 6
    , TrainerSprite 2 7
    , TrainerSprite 0 5
    , TrainerSprite 1 6
    , TrainerSprite 2 4
    , TrainerSprite 2 5
    , TrainerSprite 2 6
    , TrainerSprite 3 2
    , TrainerSprite 3 3
    , TrainerSprite 3 4
    , TrainerSprite 3 6
    , TrainerSprite 4 3
    , TrainerSprite 4 4
    , TrainerSprite 4 5
    , TrainerSprite 4 6
    , TrainerSprite 5 2
    ]


femaleSprites =
    [ TrainerSprite 5 5
    , TrainerSprite 0 0
    , TrainerSprite 0 1
    , TrainerSprite 0 4
    , TrainerSprite 1 0
    , TrainerSprite 1 1
    , TrainerSprite 1 2
    , TrainerSprite 1 7
    , TrainerSprite 2 0
    , TrainerSprite 3 5
    , TrainerSprite 5 7
    , TrainerSprite 0 3
    ]


type alias Space =
    { space : SpaceType
    , special : Special
    }


type SpaceType
    = City CitySpace
    | Event EventSpace
    | Item ItemSpace
    | Pokemon PokemonSpace


type alias CitySpace =
    { name : String
    }


type alias EventSpace =
    { name : String
    , description : String
    , sprites : List Sprite
    }


type alias ItemSpace =
    { name : String
    , description : String
    , sprite : Sprite
    }


type alias PokemonSpace =
    { name : String
    , description : String
    , roll : Int
    , sprite : Sprite
    , unique : Bool
    }


type Special
    = None
    | Cave
    | Water
    | Locked


board : List Space
board =
    [ mewtwo
    , abra
    , ssTicket
    , teamRocketThief
    , zubat
    , sandshrew
    , clefairy
    , greatBall
    , voltorb
    , machop
    , rockTunnel
    , jigglypuff
    , magikarp
    , meowth
    , onix
    , oldAmber
    , pokemonDayCare
    , zapdos
    , mrMime
    , mankey
    , nameRater
    , victoryRoad
    , pikachu
    , firstEvolution
    , haunter
    , moltres
    , weedle
    , slots
    , fightingDojo
    , silphCo
    , growlithe
    , expShare
    , nidoran
    , viridianCity
    , roulette
    , bellsprout
    , pokemonTower
    , pokemonAcademy
    , celadonDepartmentStore
    , bicycle
    , ultraBall
    , oakParcel
    , secondEvolution
    , ssAnne
    , goldeen
    , pidgey
    , fastLane
    , drowzee
    , dugtrio
    , snorlax
    , pokedex
    , eeveelutions
    , superRod
    , rattata
    , pinsir
    , scyther
    , kangaskhan
    , venonat
    , ditto
    , psyduck
    , dratini
    , pokemonMansion
    , tentacool
    , pokemonLab
    , articuno
    , jynx
    , lapras
    ]


mewtwo =
    { space =
        Pokemon
            { name = "Mewtwo"
            , description = "Psychic - Move yourself 5 spaces, move 2 other players 5 spaces and give 5 drinks"
            , roll = 5
            , sprite = PokemonSprite 6 6
            , unique = True
            }
    , special = None
    }


abra =
    { space =
        Pokemon
            { name = "Abra"
            , description = "Teleport - Teleport to a city of your choice"
            , roll = 3
            , sprite = PokemonSprite 1 4
            , unique = True
            }
    , special = None
    }


zubat =
    { space =
        Pokemon
            { name = "Zubat"
            , description = "Leech Life - Choose a player. He takes your next 3 drinks"
            , roll = 2
            , sprite = PokemonSprite 0 7
            , unique = False
            }
    , special = Cave
    }


sandshrew =
    { space =
        Pokemon
            { name = "Sandshrew"
            , description = "Sand-Attack - Evade the next 2 times you need to drink"
            , roll = 3
            , sprite = PokemonSprite 1 2
            , unique = False
            }
    , special = Cave
    }


clefairy =
    { space =
        Pokemon
            { name = "Clefairy"
            , description = "Metronome - Choose a player. He rolls a die and takes that many drinks"
            , roll = 2
            , sprite = PokemonSprite 1 3
            , unique = False
            }
    , special = Cave
    }


voltorb =
    { space =
        Pokemon
            { name = "Voltorb"
            , description = "Selfdestruct - Take 2 drinks and choose a player who must drink 6"
            , roll = 2
            , sprite = PokemonSprite 1 5
            , unique = False
            }
    , special = None
    }


machop =
    { space =
        Pokemon
            { name = "Machop"
            , description = "Submission - Give 5 drinks, then drink 2 unless your target finishes their drink"
            , roll = 4
            , sprite = PokemonSprite 1 6
            , unique = False
            }
    , special = Cave
    }


jigglypuff =
    { space =
        Pokemon
            { name = "Jigglypuff"
            , description = "Sing - Choose a player that loses a turn"
            , roll = 3
            , sprite = PokemonSprite 0 6
            , unique = False
            }
    , special = None
    }


magikarp =
    { space =
        Pokemon
            { name = "Magikarp"
            , description = "Splash - No effect!"
            , roll = 1
            , sprite = PokemonSprite 5 7
            , unique = False
            }
    , special = Cave
    }


meowth =
    { space =
        Pokemon
            { name = "Meowth"
            , description = "Pay Day - Steal an item from another player"
            , roll = 4
            , sprite = PokemonSprite 1 7
            , unique = False
            }
    , special = None
    }


onix =
    { space =
        Pokemon
            { name = "Onix"
            , description = "Sandstorm - At the start of your turn, roll a die. If you get 3 or higher, all other players take 1 drink and you repeat this at the start of your next turn"
            , roll = 2
            , sprite = PokemonSprite 0 5
            , unique = False
            }
    , special = Cave
    }


zapdos =
    { space =
        Pokemon
            { name = "Zapdos"
            , description = "Thunder - Choose a player that will roll a die before his next 3 turns.<br>1-3: Paralyzed, drink 2 and skip your turn<br>4-6: Nothing happens"
            , roll = 5
            , sprite = PokemonSprite 6 1
            , unique = True
            }
    , special = Cave
    }


mrMime =
    { space =
        Pokemon
            { name = "Mr. Mime"
            , description = "Substitute - When you have to take drinks, choose another player that will drink them for you"
            , roll = 3
            , sprite = PokemonSprite 4 6
            , unique = False
            }
    , special = None
    }


mankey =
    { space =
        Pokemon
            { name = "Mankey"
            , description = "Thrash - Give 2 drinks at the start of your next 3 turns but you cannot use another Pokémon during that time"
            , roll = 3
            , sprite = PokemonSprite 2 0
            , unique = False
            }
    , special = None
    }


pikachu =
    { space =
        Pokemon
            { name = "Pikachu"
            , description = "Double Team - Use 2 dice for your next movement roll. You may choose any combination of those dice for your movement"
            , roll = 3
            , sprite = PokemonSprite 1 1
            , unique = False
            }
    , special = None
    }


haunter =
    { space =
        Pokemon
            { name = "Haunter"
            , description = "Night Shade - You must use this immediately. Give a player twice as many drinks as the number of Pokémon they have captured"
            , roll = 3
            , sprite = PokemonSprite 2 7
            , unique = False
            }
    , special = None
    }


moltres =
    { space =
        Pokemon
            { name = "Moltres"
            , description = "Sky Attack - Choose a player that loses a turn and drinks 8"
            , roll = 5
            , sprite = PokemonSprite 6 3
            , unique = True
            }
    , special = None
    }


weedle =
    { space =
        Pokemon
            { name = "Weedle"
            , description = "String Shot - Halve another player's next movement roll (rounding up)"
            , roll = 2
            , sprite = PokemonSprite 1 0
            , unique = False
            }
    , special = None
    }


growlithe =
    { space =
        Pokemon
            { name = "Growlithe"
            , description = "Ember - Pour some hot sauce in another player's drink"
            , roll = 3
            , sprite = PokemonSprite 3 1
            , unique = False
            }
    , special = None
    }


nidoran =
    { space =
        Pokemon
            { name = "Nidoran ♂"
            , description = "Poison Sting - Choose a player to poison. He has 2 turns to get to the next city otherwise he goes back to the last city he was in"
            , roll = 2
            , sprite = PokemonSprite 4 4
            , unique = False
            }
    , special = None
    }


bellsprout =
    { space =
        Pokemon
            { name = "Bellsprout"
            , description = "Growth - Add 1 to any dice roll"
            , roll = 3
            , sprite = PokemonSprite 3 4
            , unique = False
            }
    , special = None
    }


goldeen =
    { space =
        Pokemon
            { name = "Goldeen"
            , description = "Waterfall - Do a waterfall!"
            , roll = 2
            , sprite = PokemonSprite 7 2
            , unique = False
            }
    , special = None
    }


pidgey =
    { space =
        Pokemon
            { name = "Pidgey"
            , description = "Gust - Move another player one space after he has moved. He completes that space instead"
            , roll = 2
            , sprite = PokemonSprite 0 1
            , unique = False
            }
    , special = None
    }


drowzee =
    { space =
        Pokemon
            { name = "Drowzee"
            , description = "Hypnosis - Choose a player to put to sleep. He cannot play until he rolls a 4 or higher at the beginning of his turn to wake up"
            , roll = 4
            , sprite = PokemonSprite 3 6
            , unique = False
            }
    , special = None
    }


dugtrio =
    { space =
        Pokemon
            { name = "Dugtrio"
            , description = "Earthquake - All other players drink 2"
            , roll = 3
            , sprite = PokemonSprite 3 7
            , unique = False
            }
    , special = None
    }


snorlax =
    { space =
        Pokemon
            { name = "Snorlax"
            , description = "Hyper Beam - Instantly defeat a gym leader"
            , roll = 4
            , sprite = PokemonSprite 6 4
            , unique = True
            }
    , special = None
    }


rattata =
    { space =
        Pokemon
            { name = "Rattata"
            , description = "Hyper Fang - Give 3 drinks"
            , roll = 2
            , sprite = PokemonSprite 0 0
            , unique = False
            }
    , special = None
    }


pinsir =
    { space =
        Pokemon
            { name = "Pinsir"
            , description = "Guillotine - Roll a die. If you get 5 or more, choose a player that drinks 6 and goes back to the last city he was in"
            , roll = 3
            , sprite = PokemonSprite 5 1
            , unique = False
            }
    , special = None
    }


scyther =
    { space =
        Pokemon
            { name = "Scyther"
            , description = "Swords Dance - Next time you give drinks to another player, double the amount"
            , roll = 4
            , sprite = PokemonSprite 6 7
            , unique = False
            }
    , special = None
    }


kangaskhan =
    { space =
        Pokemon
            { name = "Kangaskhan"
            , description = "Dizzy Punch - Choose a player who drinks 2 and rolls a die. If he rolls 2 or less, he gets confused and must go towards the last city he was in after his movement roll"
            , roll = 3
            , sprite = PokemonSprite 7 1
            , unique = False
            }
    , special = None
    }


venonat =
    { space =
        Pokemon
            { name = "Venonat"
            , description = "Psybeam - After another player rolls a die, turn it to the side of your choice"
            , roll = 4
            , sprite = PokemonSprite 4 1
            , unique = False
            }
    , special = None
    }


ditto =
    { space =
        Pokemon
            { name = "Ditto"
            , description = "Transform - Transform into a non-unique Pokémon you've captured this game"
            , roll = 3
            , sprite = PokemonSprite 5 6
            , unique = False
            }
    , special = None
    }


psyduck =
    { space =
        Pokemon
            { name = "Psyduck"
            , description = "Confusion - Choose a player. He must slap himself in the face before his next 3 movement rolls, or drink 2 if he forgets"
            , roll = 2
            , sprite = PokemonSprite 4 0
            , unique = False
            }
    , special = None
    }


dratini =
    { space =
        Pokemon
            { name = "Dratini"
            , description = "Twister - Choose two players other than you that swap places"
            , roll = 5
            , sprite = PokemonSprite 6 2
            , unique = False
            }
    , special = Water
    }


tentacool =
    { space =
        Pokemon
            { name = "Tentacool"
            , description = "Toxic Spikes - Place some spikes on a space that isn't a city. Players that land on it drink 5"
            , roll = 2
            , sprite = PokemonSprite 4 2
            , unique = False
            }
    , special = Water
    }


articuno =
    { space =
        Pokemon
            { name = "Articuno"
            , description = "Ice Beam - Choose a player to freeze. He loses 2 turns and can't talk until the one after that"
            , roll = 5
            , sprite = PokemonSprite 6 5
            , unique = True
            }
    , special = Water
    }


jynx =
    { space =
        Pokemon
            { name = "Jynx"
            , description = "Perish Song - Play a song of your choice. The player whose turn it is when the song ends must drink 5"
            , roll = 3
            , sprite = PokemonSprite 5 0
            , unique = False
            }
    , special = Water
    }


lapras =
    { space =
        Pokemon
            { name = "Lapras"
            , description = "Surf - Move yourself one space"
            , roll = 4
            , sprite = PokemonSprite 5 2
            , unique = False
            }
    , special = Water
    }


ssTicket =
    { space =
        Item
            { name = "S.S. Ticket"
            , description = "Allows admission to the S.S. Anne"
            , sprite = ItemSprite 0 3
            }
    , special = None
    }


greatBall =
    { space =
        Item
            { name = "Great Ball"
            , description = "Add 1 to your capture rolls"
            , sprite = ItemSprite 2 2
            }
    , special = None
    }


oldAmber =
    { space =
        Item
            { name = "Old Amber"
            , description = "A piece of amber that contains the genes of an ancient Pokémon. It is clear with a reddish tint"
            , sprite = ItemSprite 1 3
            }
    , special = None
    }


bicycle =
    { space =
        Item
            { name = "Bicycle"
            , description = "You may decide before rolling to add 1 to your movement roll"
            , sprite = ItemSprite 0 1
            }
    , special = None
    }


ultraBall =
    { space =
        Item
            { name = "Ultra Ball"
            , description = "Add 2 to your capture rolls"
            , sprite = ItemSprite 0 3
            }
    , special = None
    }


pokedex =
    { space =
        Item
            { name = "Pokédex"
            , description = "Every time you capture 3 Pokémon, all other players drink 2"
            , sprite = ItemSprite 0 0
            }
    , special = None
    }


superRod =
    { space =
        Item
            { name = "Super Rod"
            , description = "Automatically capture Pokémon on water spaces. Can only be used once per space"
            , sprite = ItemSprite 1 0
            }
    , special = None
    }


teamRocketThief =
    { space =
        Event
            { name = "Team Rocket Thief"
            , description = "Take a drink from another player's drink, then drink 3 from your own in shame because they caught you"
            , sprites =
                [ TrainerSprite 2 4
                ]
            }
    , special = None
    }


rockTunnel =
    { space =
        Event
            { name = "Rock Tunnel"
            , description = "You didn't learn Flash and you're lost in the dark! Close your eyes until your next turn"
            , sprites =
                [ CustomSprite 1 0
                ]
            }
    , special = Cave
    }


pokemonDayCare =
    { space =
        Event
            { name = "Pokémon Day Care"
            , description = "If you have a Magikarp, it evolves into Gyarados!<br>Dragon Rage - All other players return to the last city they were in"
            , sprites =
                [ PokemonSprite 5 7
                , PokemonSprite 5 3
                ]
            }
    , special = None
    }


nameRater =
    { space =
        Event
            { name = "Name Rater"
            , description = "Choose a new nickname for yourself. Other players may only call you by that name otherwise they drink"
            , sprites =
                [ TrainerSprite 2 2
                ]
            }
    , special = None
    }


victoryRoad =
    { space =
        Event
            { name = "Victory Road"
            , description = "Time to test your skills before the finale! Challenge another player to a Pokémon battle. If you win, give 4 drinks. Otherwise, take 4 drinks"
            , sprites =
                [ CustomSprite 0 0
                ]
            }
    , special = None
    }


firstEvolution =
    { space =
        Event
            { name = "Evolution"
            , description = "Your Pokémon is evolving! -1 HP during Pokémon battles and +1 to your roll during player battles"
            , sprites =
                [ PokemonSprite 2 1
                ]
            }
    , special = None
    }


slots =
    { space =
        Event
            { name = "Game Corner - Slots"
            , description = "Roll 3 dice. If you get a double or a triple, give that many drinks (2x 4 is 8 drinks for example). Otherwise, take 2 drinks"
            , sprites =
                [ CustomSprite 1 1
                ]
            }
    , special = None
    }


fightingDojo =
    { space =
        Event
            { name = "Fighting Dojo"
            , description = "Do 10 push-ups to give 6 drinks. Otherwise, take 4 drinks"
            , sprites =
                [ TrainerSprite 2 6
                ]
            }
    , special = None
    }


silphCo =
    { space =
        Event
            { name = "Silph Co."
            , description = "If you're the first player to land on this space, you may immediately capture a non-unique Pokémon of your choice. Otherwise, take 2 drinks"
            , sprites =
                [ ItemSprite 3 0
                ]
            }
    , special = None
    }


expShare =
    { space =
        Event
            { name = "Exp. Share"
            , description = "Take 2 drinks for each player behind you. Give 3 drinks to each player ahead of you"
            , sprites =
                [ ItemSprite 1 2
                ]
            }
    , special = None
    }


roulette =
    { space =
        Event
            { name = "Game Corner - Roulette"
            , description = "Guess a number and roll a die. If you are correct, give 6 drinks. Otherwise, take 1 drink"
            , sprites =
                [ CustomSprite 1 1
                ]
            }
    , special = None
    }


pokemonTower =
    { space =
        Event
            { name = "Pokémon Tower"
            , description = "Take 2 drinks and choose one: give 2 drinks to all other players or give 4 drinks to one player"
            , sprites =
                [ CustomSprite 2 1
                ]
            }
    , special = None
    }


pokemonAcademy =
    { space =
        Event
            { name = "Pokémon Academy"
            , description = "Name a type. You must name a Pokémon of that type, then the player to your left, and so on. The first player who can't come up with a new one within 5 seconds drinks 3"
            , sprites =
                [ CustomSprite 0 1
                ]
            }
    , special = None
    }


celadonDepartmentStore =
    { space =
        Event
            { name = "Celadon Department Store"
            , description = "Take any number of drinks, then give twice that amount"
            , sprites =
                [ ItemSprite 2 1
                , ItemSprite 1 1
                , ItemSprite 2 0
                ]
            }
    , special = None
    }


oakParcel =
    { space =
        Event
            { name = "Oak's Parcel"
            , description = "Go back to Pallet Town"
            , sprites =
                [ ItemSprite 0 2
                ]
            }
    , special = None
    }


secondEvolution =
    { space =
        Event
            { name = "Evolution"
            , description = "Your Pokémon is evolving! -1 HP during Pokémon battles and +1 to your roll during player battles"
            , sprites =
                [ PokemonSprite 2 4
                ]
            }
    , special = None
    }


ssAnne =
    { space =
        Event
            { name = "S.S. Anne"
            , description = "Enjoy your stay on the S.S. Anne! Choose two other players that will battle for your entertainment. The loser takes 4 drinks"
            , sprites =
                [ CustomSprite 2 0
                ]
            }
    , special = Locked
    }


fastLane =
    { space =
        Event
            { name = "Fast Lane"
            , description = "No brakes! Move another 3 spaces and complete that space"
            , sprites =
                [ TrainerSprite 4 5
                ]
            }
    , special = None
    }


eeveelutions =
    { space =
        Event
            { name = "Eeveelutions"
            , description = "Roll a die. Players that are weak to the corresponding Eeveelution drink 3<br>1-2: Flareon<br>3-4: Vaporeon<br>5-6: Jolteon"
            , sprites =
                [ PokemonSprite 7 0
                , PokemonSprite 5 4
                , PokemonSprite 5 5
                ]
            }
    , special = None
    }


pokemonMansion =
    { space =
        Event
            { name = "Pokémon Mansion"
            , description = "Roll a die.<br>1-2: Lose your next turn<br>3-4: Take 3 drinks<br>5-6: Move 1, 2 or 3 spaces in either direction and complete that space"
            , sprites =
                [ TrainerSprite 2 5
                ]
            }
    , special = Locked
    }


pokemonLab =
    { space =
        Event
            { name = "Pokémon Lab"
            , description = "If you have the Old Amber, turn it into Aerodactyl!<br>Ancient Power - Use at the start of your turn to take 2 turns in a row"
            , sprites =
                [ ItemSprite 1 3
                , PokemonSprite 6 0
                ]
            }
    , special = None
    }


viridianCity =
    { space =
        City
            { name = "Viridian City"
            }
    , special = None
    }
