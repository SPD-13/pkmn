export class RTC {
    constructor(socket, messageHandler) {
        this.socket = socket;
        this.rtc = null;
        this.dataChannel = null;
        this.peer = null;
        this.openHandler = null;
        this.messageHandler = messageHandler;
        this.closeHandler = null;
    }

    createConnection(peer, openHandler) {
        this.openHandler = openHandler;
        this.connect(peer);
        this.dataChannel = this.rtc.createDataChannel(this.socket.id);
        this.setupDataChannelHandlers();
    }

    connect(peer) {
        this.peer = peer;

        this.rtc = new RTCPeerConnection({
            iceServers: []
        });

        this.rtc.onicecandidate = this.handleICECandidateEvent.bind(this);
        this.rtc.onnegotiationneeded = this.handleNegotiationNeededEvent.bind(this);
        this.rtc.oniceconnectionstatechange = this.handleICEConnectionStateChangeEvent.bind(this);
        this.rtc.onicegatheringstatechange = this.handleICEGatheringStateChangeEvent.bind(this);
        this.rtc.onsignalingstatechange = this.handleSignalingStateChangeEvent.bind(this);
    }

    send(message) {
        if (!this.dataChannel) {
            this.createConnection(message.to, () => {
                this.send(message);
            });
        }
        else {
            switch (this.dataChannel.readyState) {
                case "connecting":
                    this.openHandler = () => {
                        this.send(message);
                    };
                    break;
                case "open":
                    this.dataChannel.send(JSON.stringify(message));
                    break;
                case "closing":
                case "closed":
                    this.createConnection(message.to, () => {
                        this.send(message);
                    });
                    break;
            }
        }
    }

    setupDataChannelHandlers() {
        if (this.openHandler)
            this.dataChannel.onopen = this.openHandler;
        if (this.messageHandler) {
            this.dataChannel.onmessage = event => {
                console.log(event);
                this.messageHandler(JSON.parse(event.data));
            };
        }
        if (this.closeHandler)
            this.dataChannel.onclose = this.closeHandler;
    }

    handleMessage(message) {
        switch (message.body.type) {
            case "rtc-offer":
                this.acceptConnection(message);
                break;
            case "rtc-answer":
                this.handleAnswerMessage(message);
                break;
            case "ice-candidate":
                this.handleICECandidateMessage(message);
                break;
        }
    }

    handleNegotiationNeededEvent() {
        this.rtc.createOffer()
            .then(function (offer) {
                return this.rtc.setLocalDescription(offer);
            }.bind(this))
            .then(function () {
                this.socket.send({
                    rtc: {
                        to: this.peer,
                        body: {
                            type: "rtc-offer",
                            sdp: this.rtc.localDescription
                        }
                    }
                });
            }.bind(this))
            .catch(this.reportError);
    }

    acceptConnection(message) {
        this.connect(message.from);
        this.rtc.ondatachannel = this.receiveDataChannel.bind(this);
        const desc = new RTCSessionDescription(message.body.sdp);
        this.rtc.setRemoteDescription(desc).then(() => {
            return this.rtc.createAnswer();
        })
            .then(answer => {
                return this.rtc.setLocalDescription(answer);
            })
            .then(() => {
                this.socket.send({
                    rtc: {
                        to: this.peer,
                        body: {
                            type: "rtc-answer",
                            sdp: this.rtc.localDescription
                        }
                    }
                });
            })
    }

    handleAnswerMessage(message) {
        const desc = new RTCSessionDescription(message.body.sdp);
        this.rtc.setRemoteDescription(desc).catch(this.reportError);
    }

    receiveDataChannel(event) {
        this.dataChannel = event.channel;
        this.setupDataChannelHandlers();
    }

    handleICECandidateEvent(event) {
        if (event.candidate) {
            this.socket.send({
                rtc: {
                    to: this.peer,
                    body: {
                        type: "ice-candidate",
                        candidate: event.candidate
                    }
                }
            });
        }
    }

    handleICECandidateMessage(message) {
        const candidate = new RTCIceCandidate(message.body.candidate);
        this.rtc.addIceCandidate(candidate).catch(this.reportError);
    }

    handleICEConnectionStateChangeEvent(event) {
        console.log(event);
    }

    handleSignalingStateChangeEvent(event) {
        console.log(event);
    }

    handleICEGatheringStateChangeEvent(event) {
        console.log(event);
    }

    reportError(event) {
        console.log(event);
    }
};
