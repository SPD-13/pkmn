module Design exposing (colors, font, playerColors)

import Array
import Css exposing (fontFamilies, hex)


colors =
    { dark = hex "263238"
    , white = hex "ffffff"
    , errorText = hex "d32f2f"
    , buttonOk = hex "1de9b6"
    , buttonCancel = hex "ff80ab"
    }


font =
    fontFamilies [ "Share Tech Mono", "monospace" ]


playerColors =
    Array.fromList
        [ "d500f9"
        , "00e676"
        , "ff1744"
        , "00b0ff"
        , "ffea00"
        , "ff9100"
        ]
