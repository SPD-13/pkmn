module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Browser.Navigation as Nav
import Html.Styled as Html exposing (toUnstyled)
import Images exposing (Images)
import Page.Host as Host
import Page.Player as Player
import Ports exposing (..)
import Tuple
import Url


type alias Model =
    { images : Images, state : State, key : Nav.Key }


type State
    = Host Host.Model
    | Player Player.Model


init : Images -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( state, cmd ) =
            case url.path of
                "/host" ->
                    Tuple.mapFirst Host Host.init

                _ ->
                    ( Player Player.init, Cmd.none )
    in
    ( { images = flags, state = state, key = key }, cmd )


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | GotHostMsg Host.Msg
    | GotPlayerMsg Player.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ state } as model) =
    case ( msg, state ) of
        ( LinkClicked urlRequest, _ ) ->
            ( model, Cmd.none )

        ( UrlChanged url, _ ) ->
            ( model, Cmd.none )

        ( GotHostMsg subMsg, Host host ) ->
            let
                ( subModel, cmd ) =
                    Host.update subMsg host
            in
            ( { model | state = Host subModel }, cmd )

        ( GotPlayerMsg subMsg, Player player ) ->
            let
                ( subModel, cmd ) =
                    Player.update subMsg player
            in
            ( { model | state = Player subModel }, cmd )

        ( _, _ ) ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions { state } =
    case state of
        Host _ ->
            Sub.map GotHostMsg Host.subscriptions

        Player _ ->
            Sub.map GotPlayerMsg Player.subscriptions


view : Model -> Browser.Document Msg
view { images, state } =
    let
        body =
            case state of
                Host host ->
                    Html.map GotHostMsg (Host.view images host)

                Player player ->
                    Html.map GotPlayerMsg (Player.view images player)
    in
    { title = "Pokémon Drinking Game"
    , body = [ toUnstyled body ]
    }


main : Program Images Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
