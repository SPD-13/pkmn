module Sprites exposing (Sprite(..), htmlFromSprite, htmlFromStarter, styledHtmlFromSprite)

import Css exposing (..)
import Html.Styled exposing (Attribute, Html, div, styled)
import Html.Styled.Attributes exposing (..)
import Images exposing (Images)
import Types exposing (..)


type Sprite
    = BadgeSprite Int Int
    | CustomSprite Int Int
    | ItemSprite Int Int
    | PokemonSprite Int Int
    | TrainerSprite Int Int


dimensions =
    { badge =
        { width = 16
        , height = 16
        }
    , custom =
        { width = 192
        , height = 96
        }
    , item =
        { width = 24
        , height = 24
        }
    , pokemon =
        { width = 64
        , height = 64
        }
    , trainer =
        { width = 64
        , height = 64
        }
    }


htmlFromSprite =
    styledHtmlFromSprite []


styledHtmlFromSprite styles images sprite =
    case sprite of
        BadgeSprite row column ->
            styled (divBadge styles)
                [ backgroundImage (url images.badges)
                , backgroundPosition2 (px (toFloat column * -dimensions.badge.width)) (px (toFloat row * -dimensions.badge.height))
                ]

        CustomSprite row column ->
            styled (divCustom styles)
                [ backgroundImage (url images.custom)
                , backgroundPosition2 (px (toFloat column * -dimensions.custom.width)) (px (toFloat row * -dimensions.custom.height))
                ]

        ItemSprite row column ->
            styled (divItem styles)
                [ backgroundImage (url images.items)
                , backgroundPosition2 (px (toFloat column * -dimensions.item.width)) (px (toFloat row * -dimensions.item.height))
                ]

        PokemonSprite row column ->
            styled (divPokemon styles)
                [ backgroundImage (url images.pokemon)
                , backgroundPosition2 (px (toFloat column * -dimensions.pokemon.width)) (px (toFloat row * -dimensions.pokemon.height))
                ]

        TrainerSprite row column ->
            styled (divTrainer styles)
                [ backgroundImage (url images.trainers)
                , backgroundPosition2 (px (toFloat column * -dimensions.trainer.width)) (px (toFloat row * -dimensions.trainer.height))
                ]


htmlFromStarter : Images -> Starter -> Evolution -> (List (Attribute msg) -> List (Html msg) -> Html msg)
htmlFromStarter images starter evolution =
    case ( starter, evolution ) of
        ( Bulbasaur, First ) ->
            htmlFromSprite images (PokemonSprite 0 2)

        ( Bulbasaur, Second ) ->
            htmlFromSprite images (PokemonSprite 2 1)

        ( Bulbasaur, Third ) ->
            htmlFromSprite images (PokemonSprite 2 2)

        ( Charmander, First ) ->
            htmlFromSprite images (PokemonSprite 0 3)

        ( Charmander, Second ) ->
            htmlFromSprite images (PokemonSprite 2 3)

        ( Charmander, Third ) ->
            htmlFromSprite images (PokemonSprite 2 4)

        ( Squirtle, First ) ->
            htmlFromSprite images (PokemonSprite 0 4)

        ( Squirtle, Second ) ->
            htmlFromSprite images (PokemonSprite 2 5)

        ( Squirtle, Third ) ->
            htmlFromSprite images (PokemonSprite 2 6)


divSprite size styles =
    styled div <|
        [ Css.width (px size.width)
        , Css.height (px size.height)
        , margin4 (px (-size.height / 2)) (px 0) (px 0) (px (-size.width / 2))
        , position absolute
        , top (pct 50)
        , left (pct 50)
        , Css.property "image-rendering" "pixelated"
        ]
            ++ styles


divBadge =
    divSprite dimensions.badge


divCustom =
    divSprite dimensions.custom


divItem =
    divSprite dimensions.item


divPokemon =
    divSprite dimensions.pokemon


divTrainer =
    divSprite dimensions.trainer
