import './main.css';
import loading from './images/load.gif';
import logo from './images/pkmn-logo.png';
import badges from './images/badgeSprites.png';
import custom from './images/customSprites.png';
import items from './images/itemSprites.png';
import pokemon from './images/pkmnSprites.png';
import trainers from './images/trainerSprites.png';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';
import { RTC } from './rtc';
import { Socket } from './socket';

const app = Elm.Main.init({
  node: document.getElementById('root'),
  flags: { loading, logo, badges, custom, items, pokemon, trainers }
});
const clientConnections = new Map();
const socket = new Socket(data => {
  data.forEach(message => {
    if (message.type === "RtcMessage") {
      const from = message.from;
      if (!clientConnections.has(from)) {
        clientConnections.set(from, new RTC(socket, msg => {
          app.ports.onClientMsg.send(msg);
        }));
      }
      const rtc = clientConnections.get(from);
      rtc.handleMessage(message);
    }
    else {
      app.ports.onServerMsg.send(message);
    }
  });
});
app.ports.serverMsg.subscribe(msg => {
  socket.send(msg);
});
app.ports.clientMsg.subscribe(message => {
  const to = message.to;
  if (!clientConnections.has(to)) {
    clientConnections.set(to, new RTC(socket, msg => {
      app.ports.onClientMsg.send(msg);
    }));
  }
  const rtc = clientConnections.get(to);
  message.from = socket.id;
  rtc.send(message);
});

registerServiceWorker();
