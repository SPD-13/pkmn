# Pokemon: The Board Game

This is a game meant to be played locally by multiple players using their cell phones.
A host device such as a laptop hosts the game and players then connect to it.
Players first connect to a central server via WebSockets and ask to join a specific room.
They then connect to the game host via WebRTC.
All players are connected to the host and interactions between the players always go through the host,
which is in charge of updating the game state.
Here are the different projects in the repo:


### backend-go
A WebSocket server written in Go that holds information about the rooms currently available.
Its only purpose is to establish the connection between clients at the start of a game.


### frontend-elm
An Elm SPA which has two pages:

- The host page creates a room and runs the game
- The player page allows players to join a room and play

WebSocket and WebRTC communication is handled in JavaScript through ports.
The project is created and built using create-elm-app (webpack-based)


### json-generator-haskell

A Haskell project to parse Elm types from source using parser combinators and generate JSON encoders and decoders for the types.
This is used to serialize arbitrary data between Elm clients when communicating via WebRTC.


### simulator-haskell

An RNG simulation tool to help balance the gameplay of gym battles.


## Setup

1. Install Docker and Docker Compose
2. Install VS Code with the "Remote - Containers" extension

To work on a project, open a new window, press F1, select "Remote-Containers: Open Folder in Container..." and choose the subfolder of the desired project. This will spin up all necessary services and command-line tools will be available via the terminal.
