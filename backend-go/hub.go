// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[string]*Client

	// Inbound messages from the clients.
	message chan Message

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// Map of running games
	games map[string]string

	// Current room code
	roomCode string
}

func newHub() *Hub {
	return &Hub{
		message:    make(chan Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[string]*Client),
		games:      make(map[string]string),
		roomCode:   "BIXI",
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.id] = client
		case client := <-h.unregister:
			if _, ok := h.clients[client.id]; ok {
				delete(h.clients, client.id)
				close(client.send)
			}
		case message := <-h.message:
			if message.Rtc != nil {
				h.send(message.Rtc.To, newRtcMessage(message.client, message.Rtc.Body))
			} else if message.HostRequest != nil {
				h.games[h.roomCode] = message.client
				h.send(message.client, newHostResponse(h.roomCode))
			} else if message.JoinRequest != nil {
				host, ok := h.games[message.JoinRequest.Code]
				if ok {
					h.send(message.client, newJoinResponse(true, host))
				} else {
					h.send(message.client, newJoinResponse(false, "Room not found"))
				}
			}
		}
	}
}

func (h *Hub) send(id string, message interface{}) {
	client, ok := h.clients[id]
	if ok {
		select {
		case client.send <- message:
		default:
			close(client.send)
			delete(h.clients, id)
		}
	}
}

type HostResponse struct {
	Type string `json:"type"`
	Room string `json:"room"`
}

func newHostResponse(room string) HostResponse {
	return HostResponse{Type: "HostResponse", Room: room}
}

type OutJoinResponse struct {
	Type    string `json:"type"`
	Success bool   `json:"success"`
	Message string `json:"message"`
}

func newJoinResponse(success bool, message string) OutJoinResponse {
	return OutJoinResponse{Type: "JoinResponse", Success: success, Message: message}
}

type RtcMessage struct {
	Type string      `json:"type"`
	From string      `json:"from"`
	Body interface{} `json:"body"`
}

func newRtcMessage(from string, body interface{}) RtcMessage {
	return RtcMessage{Type: "RtcMessage", From: from, Body: body}
}
