import System.Random

numSamples = 1000
leaders = [brock]
types = [Grass, Fire, Water]
evolutions = [First, Second, Third]

main = do
    gen <- getStdGen
    let (results, nextGen) = foldr (battle brock Grass First) ([], gen) [1..numSamples]
        output = formatResults brock Grass First results
    putStrLn output

formatResults :: Leader -> Type -> Evolution -> [BattleState] -> String
formatResults leader t evolution results =
    let title = name leader ++ " " ++ show t ++ " " ++ show evolution ++ "\n"
        averageDrinks = fromIntegral (sum (map drinks results)) / fromIntegral numSamples
        numDrinks = "Average drinks : " ++ show averageDrinks ++ "\n"
        averageTurns = fromIntegral (sum (map numTurns results)) / fromIntegral numSamples
        turns = "Average turns : " ++ show averageTurns
    in title ++ numDrinks ++ turns

data Type = Grass | Fire | Water deriving Show
data Evolution = First | Second | Third deriving (Enum, Show)

type HitPoints = Int

data Status
    = None
    | Bound Int
    deriving Eq

data BattleState = BattleState
    { leaderHp :: HitPoints
    , playerHp :: HitPoints
    , drinks :: Int
    , status :: Status
    , numTurns :: Int
    , victory :: Bool
    }

type Move = Type -> BattleState -> BattleState

data Leader = Leader
    { name :: String
    , maxHealth :: HitPoints
    , moves :: [Move]
    }

four _ state = state { leaderHp = leaderHp state - 1 }

brock = Leader { name = "Brock", maxHealth = 4, moves = [brockOne, brockTwo, brockThree, four, brockFive, brockSix] }
brockOne _ state
    | status state == None = state { status = Bound 2 }
    | otherwise = state
brockTwo _ state = state { drinks = drinks state + 1 }
brockThree Fire state = state { drinks = drinks state + 2 }
brockThree _ state = state { drinks = drinks state + 1 }
brockFive Fire state = state { leaderHp = leaderHp state - 1 }
brockFive _ state = state { leaderHp = leaderHp state - 3 }
brockSix Fire state = state { leaderHp = leaderHp state - 2 }
brockSix _ state = state { leaderHp = leaderHp state - 4 }

battle :: Leader -> Type -> Evolution -> Int -> ([BattleState], StdGen) -> ([BattleState], StdGen)
battle leader t evolution _ (results, gen) =
    let health = maxHealth leader - fromEnum evolution
        state = BattleState { leaderHp = health, playerHp = 2, drinks = 0, status = None, numTurns = 0, victory = False }
        (result, nextGen) = fight (moves leader) t state gen
    in (result : results, nextGen)

fight :: [Move] -> Type -> BattleState -> StdGen -> (BattleState, StdGen)
fight ms t state gen
    | leaderHp state <= 0 = (state { victory = True }, gen)
    | playerHp state <= 0 = (state, gen)
    | otherwise =
        let statusState = handleStatus (status state) state
            (nextMove, nextGen) = randomR (0, 5) gen
            nextState = (ms !! nextMove) t statusState
            updated = nextState { numTurns = numTurns nextState + 1 }
        in fight ms t updated nextGen

handleStatus :: Status -> BattleState -> BattleState
handleStatus (Bound turns) state =
    let nextStatus =
            if turns == 1 then
                None
            else
                Bound (turns - 1)
    in state { status = nextStatus, drinks = drinks state + 1 }
handleStatus None state = state
